package codingTask;

import junit.framework.TestCase;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {

    @Test
    public void testFileProcessing() throws IOException {

        assertEquals(Paths.get("/files/input/input.txt").getFileName(), Paths.get("input.txt"));
        assertTrue(Files.isSameFile(Paths.get("/files/input/input.txt").getFileName(), Paths.get("input.txt")));

        App app = new App();
        app.process("input.txt", "OUTPUT.TXT");
        Path srcPath = Paths.get("src/test/resources/files/output/output.txt");
        assertTrue(srcPath != null);
        assertTrue(Files.exists(srcPath));
        assertEquals(srcPath.getNameCount(), 6);

        System.out.println(Files.readAllLines(srcPath));
    }

}
