package codingTask;

import java.io.IOException;

/**
 * Date: 06.10.2016
 * Time: 19:55
 */
public interface Processor {
    void process(String srcFilePath, String destFilePath) throws IOException;
}
