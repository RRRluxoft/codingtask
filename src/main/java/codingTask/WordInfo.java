package codingTask;

import java.util.Set;
import java.util.TreeSet;

class WordInfo implements Comparable<WordInfo> {
    private static final String DELIMITER = ", ";

    private static final char[] VOWELS_CHARS = "AEIOUaeiou".toCharArray();

    private final Set<Character> vowels = new TreeSet<>();
//    private final Set<Character> immutableVowels = Collections.unmodifiableSet(vowels);

    private int wordLength;
    private int vowelsCount;

    public boolean isEmpty() {
        return wordLength == 0;
    }

//    public Set<Character> getVowels() {
//        return immutableVowels;
//    }

    public int getVowelsCount() {
        return vowelsCount;
    }

    public int getWordLength() {
        return wordLength;
    }

    public boolean addVowel(int chInt) {
        boolean isVowel = false;
        for (char ch : VOWELS_CHARS) {
            if (ch == (char)chInt) {
                isVowel = true;
                break;
            }
        }
        if (isVowel) {
            vowels.add((char) chInt);
            ++vowelsCount;
        }
        return isVowel;
    }

    public void incrementWordLength() {
        ++wordLength;
    }

    @Override
    public int compareTo(WordInfo wordInfo) {
        final int LESS = -1;
        final int SAME = 0;
        final int MORE = 1;

        if (this == wordInfo || this.wordLength == wordInfo.wordLength) {
            return SAME;
        }
        return this.wordLength > wordInfo.wordLength ? MORE : LESS;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        WordInfo data = (WordInfo) obj;
        return wordLength == data.wordLength && vowels.equals(data.vowels);
    }

    @Override
    public int hashCode() {
        return wordLength ^ vowels.hashCode();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder().append("({");
        if (!vowels.isEmpty()) {
            for (Character ch : vowels) {
                sb.append(ch).append(DELIMITER);
            }
            sb.delete(sb.length() - DELIMITER.length(), sb.length());
        }
        return sb.append('}').append(DELIMITER).append(wordLength).append(')').toString();
    }

}

