package codingTask;

import com.google.common.base.Charsets;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class App implements Processor {

    private static final String INPUT_FILE_PATH = "/files/input/";
    private static final String OUTPUT_FILE_PATH = "src/test/resources/files/output/";

    private StringWriter writer = new StringWriter();

    @Override
    public void process(String srcFilePath, String destFilePath) {
        try (Writer writer = createWriter()) {
            File file = getFileByName(srcFilePath);
            String content = getContentFromFile(file);
            List<WordInfo> words = readWords(content);
            Map<WordInfo, List<WordInfo>> groups = groupWords(words);

            contentPreparing(writer, groups);

            writeToFile(writer.toString(), OUTPUT_FILE_PATH.concat(destFilePath));

        } catch (FileNotFoundException ex){
            ex.getMessage();
        } catch (IOException ex) {
            ex.getMessage();
        }
    }

    private Writer createWriter() {
        return writer;
    }

    private File getFileByName(String srcFilePath) {
        return new File(getClass().getResource(INPUT_FILE_PATH.concat(srcFilePath)).getFile());
    }

    private String getContentFromFile(File file) throws IOException {
        return new String(Files.readAllBytes(Paths.get(file.toURI())));
    }

    private List<WordInfo> readWords(String src) throws IOException {
        List<WordInfo> wordInfoList = new ArrayList<>();
        try (InputStream stream = createInputStream(src); InputStreamReader reader = new InputStreamReader(stream)) {
            WordInfo wordInfo = null;
            int ch;
            while ((ch = reader.read()) > -1) {
                if (Character.isLetter(ch)) {
                    if (wordInfo == null) {
                        wordInfo = new WordInfo();
                    }
                    wordInfo.addVowel((char) ch);
                    wordInfo.incrementWordLength();
                }
                else {
                    if (wordInfo != null && !wordInfo.isEmpty()) {
                        wordInfoList.add(wordInfo);
                        wordInfo = null;
                    }
                }
            }
            if (wordInfo != null && !wordInfo.isEmpty()) {
                wordInfoList.add(wordInfo);
            }
        }
        return wordInfoList;
    }

    private void writeToFile(String writer, String destFileName) throws IOException {
        Path path = Paths.get(destFileName);
        Files.write(path, writer.getBytes());
    }

    private void contentPreparing(Writer writer, Map<WordInfo, List<WordInfo>> groups) throws IOException {
        for (Map.Entry<WordInfo, List<WordInfo>> entry : groups.entrySet()) {
            writer.append(entry.getKey().toString()).
                append(" -> ").
                append(Double.toString(calculateResult(entry.getValue()))).
                append('\n');
        }
    }

    private InputStream createInputStream(String src) throws IOException {
        return new ByteArrayInputStream(src.getBytes(Charsets.UTF_8));
    }

    private Map<WordInfo, List<WordInfo>> groupWords(List<WordInfo> wordInfoList) {
        Map<WordInfo, List<WordInfo>> wordsGroup = new HashMap<>();
        for (WordInfo wordInfo : wordInfoList) {
            List<WordInfo> words = wordsGroup.get(wordInfo);
            if (words == null) {
                words = new ArrayList<>();
                wordsGroup.put(wordInfo, words);
            }
            words.add(wordInfo);
        }
        return wordsGroup;
    }

    private static Double calculateResult(List<WordInfo> words) {
        if (words.isEmpty()) {
            return 0D;
        }
        double vowelsCount = 0D;
        for (WordInfo word : words) {
            vowelsCount += word.getVowelsCount();
        }
        return vowelsCount / words.size();
    }

}
